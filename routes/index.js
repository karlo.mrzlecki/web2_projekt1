var router = require('express').Router();
const { requiresAuth } = require('express-openid-connect');

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Projekt1',
    isAuthenticated: req.oidc.isAuthenticated()
  });
});

router.get('/maps', requiresAuth(), function (req, res, next) {
  res.render('maps', {
    userProfile: JSON.stringify(req.oidc.user, null, 2),
    title: 'Map with user info'
  });
});

module.exports = router;
